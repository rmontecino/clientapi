﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClientAPI
{
    class Program
    {
        static void Main(string[] args)
        {
            var s = MakeRequest();
            Console.WriteLine(String.Format("Respuesta API : {0}", s));
            Console.WriteLine("Presiona ENTER para salir...");
            Console.ReadLine();
        }

        static string MakeRequest()
        {
            System.Net.WebClient client = new System.Net.WebClient();
            client.Headers.Add("Ocp-Apim-Subscription-Key", "a49f59e40001430180df34be9076e405");
            Uri uri = new Uri("https://facturaya.azure-api.net/siiservices/EnvioDTE?type=xml");
            byte[] byteData = Encoding.UTF8.GetBytes("<?xml version='1.0' encoding='ISO-8859-1'?><DTE version='1.0'><Documento ID = 'T33F1'></Documento></DTE>");
            string s = Encoding.UTF8.GetString(client.UploadData(uri, "POST", byteData));
            return s;
        }
    }
}
