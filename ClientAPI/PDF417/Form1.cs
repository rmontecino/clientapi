﻿using Facturaya;
using System;
using System.Text;
using System.Windows.Forms;

namespace PDF417
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void btnImprimir_Click(object sender, EventArgs e)
        {
            var firma = "<TED version=\"1.0\"><DD><RE>6284657-7</RE><TD>33</TD><F>11</F><FE>2018-01-11</FE><RR>77206480-2</RR><RSR>Comercial Tres Almendros Ltda.</RSR><MNT>178500</MNT><IT1>Molida Magra</IT1><CAF version=\"1.0\"><DA><RE>6284657-7</RE><RS>BERTA DEL CARMEN GUZMAN AQUILES</RS><TD>33</TD><RNG><D>1</D><H>1000</H></RNG><FA>2016-02-04</FA><RSAPK><M>wegC8x8/a3hJRLrI0pJrZzkmMfziUByyzcIPWdUn9u7GpRXmKFV2dyF33OGV38rEzG0Y5hEy4nHW5LZVwM9W1w==</M><E>Aw==</E></RSAPK><IDK>100</IDK></DA><FRMA algoritmo=\"SHA1withRSA\">f9qiPY+ZQxlkNCZV72k+ghqKxNx9UmehUXdwapEZLLfAqO3k6SmAhtgGM6GuwrC9BXw3dypPkbrzVuRJi8R0Xw==</FRMA></CAF><TSTED>2018-01-31T03:07:57</TSTED></DD><FRMT algoritmo=\"SHA1withRSA\">fziXRSrJaY1a/bF4Nb5P7oMPbgVvvEiHL7JsUoOEbUG3As+y8SNrtgG9fauZUUIJLGIq8WRQYbaEGDXUA9KC0w==</FRMT></TED>";
            Pdf417lib pd = new Pdf417lib();
            Encoding encoding = Encoding.GetEncoding("ISO-8859-1");
            Byte[] bytes = encoding.GetBytes(firma.ToCharArray());
            sbyte[] sbytes = (sbyte[])(Array)bytes;
            pd.setText(sbytes);
            pd.ErrorLevel = 5;
            pd.Options = Pdf417lib.PDF417_INVERT_BITMAP;
            pd.paintCode();
            this.pcbPDF417.Image = pd.DrawBarcodeImage(pd, 1, 1);
        }
    }
}
